class NewTeam:

    def __init__(self, city, name, confrence, division, rating):
        self.city = city
        self.name = name
        self.confrence = confrence
        self.division = division
        self.rating = int(rating)
        self.schedule = dict() 
        self.stats = dict()
        self.stats = {"played":0, "wins":0, "losses":0, 
                      "otlosses":0, "points":0, "percent":0.000, "streak":0, "cups":0}

    def get_streak(self):
        return self.stats["streak"]

    def update_stats(self, entry):
        self.stats[entry] += 1
        if entry == "wins":
            self.stats["points"] += 2
            self.stats["streak"] += 1
        elif entry == "otlosses":
            self.stats["points"] += 1
        elif entry == "losses":
            self.stats["streak"] -= 1
        try:
            percent = float(self.stats["points"]) / float(self.stats['played'] * 2)
        except ZeroDivisionError:
            percent = 0.000
        self.stats["percent"] = "%.3f" % percent
