def genteams():
    import newteam

    teams = {} 
    file_teams = open("teams.dat", 'r')
    for entry in file_teams:
        entry = entry.split('\t')
        teamX = newteam.NewTeam(entry[0], entry[1], entry[2],
                                 entry[3], entry[4])
        teams[entry[0]+' '+entry[1]] = teamX

    file_teams.close()
    return teams
