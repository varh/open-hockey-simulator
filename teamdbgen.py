raw = """
    Anaheim Ducks
    Arizona Coyotes
    Boston Bruins
    Buffalo Sabres
    Calgary Flames
    Carolina Hurricanes
    Chicago Blackhawks
    Colorado Avalanche
    Columbus BlueJackets
    Dallas Stars
    Detroit RedWings
    Edmonton Oilers
    Florida Panthers
    LosAngeles Kings
    Minnesota Wild
    Montreal Canadiens
    Nashville Predators
    NewJersey Devils
    NewYork Islanders
    NewYork Rangers
    Philadelphia Flyers
    Pittsburgh Penguins
    Ottawa Senators
    SanJose Sharks
    StLouis Blues
    TampaBay Lightning
    Toronto MapleLeafs
    Vancouver Canucks
    Washington Capitals
    Winnipeg Jets
"""

teams = ""

cut = raw.split()

a = 0
for i in cut:
    if a:
       teams += i+"\n"
       a = 0
    else:
       teams += i + " "
       a = 1

data_file = open("teams.db", "w")
data_file.write(teams)
data_file.close()
