#!/bin/python2

import game
import genteams
import random

instance = game.Game()

schedule_file = open("schedule.dat", 'r')

for line in schedule_file:
    line = line.split(',')
    away_name = line[2]
    home_name = line[4]
    instance.simulate_game(away_name, home_name)

unsorted = []
for team_name in instance.dict_teams:
    points = instance.dict_teams[team_name].stats["points"]
    unsorted.append([points, team_name])

def sort(unsorted):
    unsort = unsorted
    sorted = []
    
    for i in range(len(unsorted)):
        max_unsorted = max(unsort)
        sorted.append(max_unsorted)
        del(unsort[unsort.index(max_unsorted)])
    return sorted

sorted = sort(unsorted)

tab = '\t'
print "%15s GP\tW\tL\tOT\tPT\t%%\t SK" %"TEAM"

for i in sorted:
    team_name = i[1]
    team = instance.dict_teams[team_name]
    print "%15s" %team.name,
    team_stats = team.stats
    print team_stats['played'], tab, team_stats['wins'], tab,
    print team_stats['losses'], tab, team_stats['otlosses'], tab,
    print team_stats['points'], tab, team_stats['percent'], tab,
    print "%3s" %str(team_stats['streak'])
