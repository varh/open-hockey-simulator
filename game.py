import random
import sys

class Game:

    def __init__(self, runtime=1):
        import genteams
        self.dict_teams = genteams.genteams()

    def simulate_game(self, away_name, home_name):
        FACTOR = 2
        away = self.dict_teams[away_name]
        home = self.dict_teams[home_name]
        away_rating = away.rating + away.get_streak()*FACTOR
        home_rating = home.rating + home.get_streak()*FACTOR
        rating_ratio =  float(away.rating)/float(home.rating)
        ratio = 50 * rating_ratio
        outcome = random.randint(0,99)

        away.update_stats("played")
        home.update_stats("played")
        
        if outcome < ratio:
            if outcome < (.2 * ratio):
                away.update_stats("wins")
                home.update_stats("otlosses")
            else:
                away.update_stats("wins")
                home.update_stats("losses")
        elif outcome >= ratio:
            if outcome > ( 100 - .2 * ratio ):
                away.update_stats("otlosses")
                home.update_stats("wins")
            else:
                home.update_stats("wins")
                away.update_stats("losses")

    def sim_next(self):
        pass

    def print_stats(self, team):
        print team.stats

    def quit(self):
        sys.exit(0)    # Successful termination
